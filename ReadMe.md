Keydown Events
For this assessment, you will create a simple web page that moves a box in response to keydown events.

Steps
Create a new repository.
Copy the example code in the MDN documentation for keydown event into a new index.html file and main.js file and link them using a script tag.
Open index.html in your browser.
Click on the page then try pressing the cursor (arrow) keys on your keyboard. What key names are shown? You can also refer to the Key Values documentation to find the names associated with various keys.
Add a div with id="box" inside the body element of the page, and apply the following CSS rule:
#box { 
    width: 100px; 
    height: 100px; 
    position: absolute; 
    background: 
    gray; 
    left: 200px; 
    top: 200px; 
}
Initialize two new global variables outside the function logKey:
let boxTop = 200;
let boxLeft = 200;
Modify the logKey function so that the key named "ArrowDown" adds 10 to the boxTop variable. Similarly, make "ArrowUp" subtract 10 from the boxTop variable.
Add the following line to the end of the logKey function to update the "top" style attribute of the box:
document.getElementById("box").style.top = boxTop + "px";
Similarly, add code to the logKey function so that the left and right cursor keys modify the "left" style attribute of the box.

You should now be able to move the box around the page using your arrow keys.

Finally, replace the boring gray box with an image.

I used Code we went over in Erics 1 O'clock supplemental instruction 3/18/20
